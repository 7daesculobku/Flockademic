import { GetPeriodicalContentsResponse } from '../../../../lib/interfaces/endpoints/periodical';
import { Request } from '../../../../lib/lambda/faas';
import { DbContext } from '../../../../lib/lambda/middleware/withDatabase';
import { fetchPeriodicalContents as commandHandler } from '../services/fetchPeriodicalContents';

export async function fetchPeriodicalContents(
  context: Request<undefined> & DbContext,
): Promise<GetPeriodicalContentsResponse> {
  if (!context.params || context.params.length < 2) {
    throw(new Error('Could not find journal articles without a journal ID.'));
  }

  try {
    const periodical = await commandHandler(context.database, context.params[1]);

    return periodical;
  } catch (e) {
      throw new Error(`Could not find articles for the journal with ID \`${context.params[1]}\`.`);
  }
}
