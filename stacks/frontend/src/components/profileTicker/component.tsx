require('./styles.scss');

import * as React from 'react';

import { Person } from '../../../../../lib/interfaces/Person';
import { getOrcidAuthors } from '../../services/crossref';
import { ProfileList } from '../profileList/component';

interface State {
  profiles?: null | Array<Partial<Person>>;
}

interface Props {}

export class ProfileTicker
  extends React.Component<
  Props,
  State
> {
  public state: State = {};

  constructor(props: any) {
    super(props);
  }

  public async componentDidMount() {
    try {
      const profiles = await getOrcidAuthors();

      this.setState({ profiles: profiles.slice(0, 5) });
    } catch (e) {
      this.setState({ profiles: null });
    }
  }

  public render() {
    if (!this.state.profiles || this.state.profiles.length < 5) {
      // Do not render anything when nothing could be fetched - this component is non-essential.
      // Also do not render with fewer than five items, as the fade-oud effect would be odd.
      return null;
    }

    return (
      <section className="hero">
        <div className="hero-body">
          <div className="container profileTicker">
            {/* <p className="has-text-centered">Some Flockademic profiles</p> */}
            <ProfileList
              profiles={this.state.profiles}
            />
          </div>
        </div>
      </section>
    );
  }
}
