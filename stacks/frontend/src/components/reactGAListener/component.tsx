/*
 * This component should be added inside a Router component from React Router.
 * It will listen to route changes and send them to Google Analytics.
 */

// `Location` is used by react-router, so as long as we're using React Router,
// we can be sure that `history` is present as well:
// tslint:disable-next-line:no-implicit-dependencies
import { Location } from 'history';
import * as PropTypes from 'prop-types';
import * as React from 'react';
import * as ReactGA from 'react-ga';
import { RouterChildContext } from 'react-router';

// tslint:disable:object-literal-sort-keys
const gaDimensions = {
  GA_CONFIG_VERSION: 'dimension1',
  CODE_BRANCH: 'dimension2',
};
// tslint:enable:object-literal-sort-keys

// Since this is hard to test with JSDOM's polyfills, and it's only secondary functionality,
// it's OK to skip tests for this:
/* istanbul ignore next */
if (
  (typeof navigator !== 'undefined' && typeof window !== 'undefined') &&
  // The TypeScript DOM typings don't include the doNotTrack header at the time of writing,
  // so convert it to `any` for the time being. See
  // https://github.com/Microsoft/TypeScript/issues/19024
  (!(navigator as any).doNotTrack || (navigator as any).doNotTrack.substr(0, 1) !== '1') &&
  (!window.doNotTrack || window.doNotTrack.substr(0, 1) !== '1')
) {
  ReactGA.initialize('UA-84089544-2');
  ReactGA.set({
    // https://philipwalton.com/articles/the-google-analytics-setup-i-use-on-every-site-i-build/#loading-analyticsjs
    transport: 'beacon',
    // https://philipwalton.com/articles/the-google-analytics-setup-i-use-on-every-site-i-build/#tracking-custom-data
    [gaDimensions.GA_CONFIG_VERSION]: '1',
    [gaDimensions.CODE_BRANCH]: process.env.CODE_BRANCH,
  });
}

export class ReactGAListener extends React.Component<{}, {}> {
  // React Router passes the router down through React's "context" API:
  // https://facebook.github.io/react/docs/context.html
  // We need to access that to be able to listen to route changes globally.
  public static contextTypes = {
    router: PropTypes.object,
  };
  public context!: RouterChildContext<{}>;

  public componentDidMount() {
    sendPageView(this.context.router.history.location);
    this.context.router.history.listen(sendPageView);
  }

  public render() {
    return <div>{this.props.children}</div>;
  }
}

function sendPageView(location: Location) {
  ReactGA.set({ page: location.pathname });
  ReactGA.pageview(location.pathname);
}
