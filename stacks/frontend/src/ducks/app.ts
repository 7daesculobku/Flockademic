import { AnyAction, combineReducers } from 'redux';

import { AccountState, reducer as account  } from './account';
import { AlertState, reducer as alert } from './alert';

// See https://github.com/acdlite/flux-standard-action
export interface FluxStandardAction extends AnyAction {
  payload?: Error | any;
  error?: boolean;
  meta?: any;
}

export interface AppState {
  account: AccountState;
  alert: AlertState;
}

export const AppReducer = combineReducers<AppState>({ alert, account });
