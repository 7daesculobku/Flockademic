import { GetOrcidAuthorsResponse, isValidOrcidAuthorsResponse } from '../../../../lib/interfaces/endpoints/crossref';
import { Person } from '../../../../lib/interfaces/Person';

interface Author {
  name: string;
  sameAs: string;
}

export async function getOrcidAuthors(): Promise<Array<Partial<Person>>> {
  const response = await fetch(
    // tslint:disable-next-line:max-line-length
    'https://api.crossref.org/works?filter=has-authenticated-orcid:true,license.url:https%3A%2F%2Fcreativecommons.org%2Flicenses%2Fby%2F4.0%2F&sample=5&mailto=api.crossref.org@Flockademic.com',
  );

  const data: GetOrcidAuthorsResponse = await response.json();

  if (!isValidOrcidAuthorsResponse(data)) {
    throw new Error('Could not fetch a list of authors.');
  }

  return data.message.items.reduce(
    (soFar, item) => {
      return soFar.concat(
        item.author
        .filter((author) => (typeof author.ORCID !== 'undefined') && author['authenticated-orcid'])
        .map((author) => ({
          // We're assuming the names to be ordered Western-style here, which doesn't always hold.
          // Unfortunately, I'm not quite sure how to deduce the appropriate order at this time.
          name: `${author.given} ${author.family}`,
          // We have to explicitly (through `!`) tell TypeScript that ORCID is not undefined, thanks to `filter`.
          // Furthermore, CrossRef at this time returns HTTP links, whereas we only use HTTPS links
          sameAs: author.ORCID!.replace(/^http:/, 'https:'),
        })) as Author[],
      );
    },
    [] as Author[],
  );
}
