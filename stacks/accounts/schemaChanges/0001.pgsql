---------------------------------------------------------
--                      WARNING                        --
-- Do not modify this script after merging it to       --
-- `dev`. Only add new, consecutively numbered         --
-- changesets in case the schema needs further         --
-- changes.                                            --
---------------------------------------------------------

-- NOTE: When running locally. Docker Compose won't pick up changes in SQL files unless you recreate the volume:
--       docker-compose rm -v; docker-compose up --build;

BEGIN;
    CREATE TABLE IF NOT EXISTS orcids (
      orcid char(19) NOT NULL PRIMARY KEY,
      access_token char(36) NOT NULL,
      refresh_token char(36) NOT NULL,
      name text,
      authenticated_datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
    );

    CREATE TABLE IF NOT EXISTS accounts (
      identifier UUID PRIMARY KEY,
      create_datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
      orcid char(19) references orcids(orcid)
    );
    CREATE INDEX accounts_orcid_index ON accounts(orcid);

    CREATE TABLE IF NOT EXISTS sessions (
      identifier UUID PRIMARY KEY,
      account UUID REFERENCES accounts(identifier),
      create_datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
      refresh_token char(64)
    );
    CREATE INDEX sessions_account_index ON sessions(account);
COMMIT;

