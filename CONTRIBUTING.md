The setup of Flockademic is designed with three core principles: new ideas should be able to quickly and reliably be tested, stacks should be able to be maintained independently, and tooling should help prevent the developers from making mistakes without being in the way.

Note that these principles were determined at the start of the project, and although we should make an effort to stick to them, they are not set in stone. Hence, if you feel that, even after trying, adhering to these principles is too limiting or just not feasible in practice, feel free to bring that up.

# Quick iteration

In order to move quickly and to easily be able to validate ideas, we should be able to quickly spin up new code in an environment as close to production as possible - by way of [Review apps](https://about.gitlab.com/features/review-apps/). Environment for new features should be independent of each other, and fully functional. This will allow us to move faster while minising the risk of deploying buggy code to production.

As a desirable side effect, this also forces us to maintain proper demo data. This lowers the barrier to entry for potential new users (as they can try it out without having to e.g. register). It also enables us to write proper automatic confidence checks.

# Independent maintenance

The goal of this tenet is to keep maintenance work small and contained. In other words, small fixes in one stack should not affect the other stacks, unless they affect its API - and we should strive to keep the API backwards-compatible.

In concrete terms, this means that stacks are deployed to e.g. separate AWS Lambda functions and should not share access to the same database other than through each other's public API's. That said, they do move through the different environments together to support the case of changing API's. It also means that we should be able to e.g. upgrade the version of a library used in one stack without having to do so in the other right away (although regularly updating dependencies is, of course, a good idea for every stack - we just want to keep the individual manageable).

# Tooling that prevents mistakes

We use relatively strict configuration of linters, test coverage, etc. This is not to be dogmatic, however. Rather, developers should be conscious of where the rules should be loosened, and explicitly add exceptions at those places together with a comment explaining why the exception is justified. This ensures that those cases remain understable to readers of the code in the future, and also makes sure that we can clearly differentiate between places where cleanup was forgotten and places that were explicitly skipped.

For example, let's say that a specific code path is untested. Since the coverage requirement is 100%, we will be notified of this, whereas the coverage difference between 93.7% and 94.2% can easily slip by unnoticed. If, however, the code is impossible to test and cannot be refactored to make it testable, a simple `/* istanbul ignore next */` and a comment explaining why that was added can fix it.

Note that locally, the developer should be able to quickly prototype and iterate. Thus, coverage, linting and other rules should only be warnings during development and only enforces on commit. Furthermore, developers working in separate feature branches that are still Work In Progress are free to commit with `--no-verify` and clean it up before they do a merge request, to encourage unfinished work to be pushed often. After all, the checks will again be run in CI Pipelines before the merge request is accepted. Do make sure to [clean up the commit history](https://vincenttunru.com/Spend-effort-on-your-Git-commits/) before submitting a merge request, though.

# Running the app

The easiest way to run the app is if you have [Docker](https://www.docker.com/) and [Docker Compose](https://docs.docker.com/compose/) installed. You should then be able to enter the top-level directory and to run `docker-compose up`, which will start the app at http://localhost:8000/. Note that this can take a while, especially initially.

Docker is required to set up a local database locally, but if you so desire, you can run the app locally without a backing database as well. To do so, we use [Node](https://nodejs.org/) as the runtime and [Yarn](https://yarnpkg.com/) as the package manager, at the time of writing, so make sure you have those installed. Run `yarn install` in the top-level directory to fetch all required dependencies. Tests can then be run with `yarn test`, and `yarn start` runs the API and subsequently the front-end, pointed to the running API.

In fact, it is also possible to run just the front-end code without the API (although of course, API calls will fail). To do so, simply enter the `/stacks/frontend` directory, run `yarn install` once to install all required dependencies, and then `yarn start` to run the front-end.

# Contributing to the code

To make a change, it's best first [create an issue](https://gitlab.com/Flockademic/Flockademic/issues/new) against the Flockademic project, describing the desired change. This will provide a place for you to gather feedback on how to approach the specific problem.

New feature branches should be based on the `dev` branch, which should always hold the latest version of the code and remain stable. When [submitting a merge request](https://gitlab.com/Flockademic/Flockademic/merge_requests/new), either select the `dev` branch as the target branch, or a branch created specifically for you when you created the above issue. Merged branches will automatically be deployed to `<branch name>.flockademic.com`, so you can get a feel for what it looks like in a production-like environment.

Also note that by submitting a pull request, you are agreeing for your contributions to be published under the same license as the rest of the code.

# The stacks

At the likely risk of this getting outdated, a quick overview of the different stacks and their purposes. Note that we're still figuring out the ideal stack size in terms of the balance between bite-sized maintenance work and the extent to which it is limiting during development. At the moment, stacks roughly conform to a stack per API resource, and one for the front-end. However, we might prefer to move to a single stack per resource method, or to e.g. collapse all API resources into a single back-end stack.

Consult `stacks/<stack name>/CONTRIBUTING.md` for information on each stack's different parts.

The stacks are:

# frontend
This stack holds the front-end code; at the time of writing, this is a placeholder page to give the project some web presence and enable people to start following it.
